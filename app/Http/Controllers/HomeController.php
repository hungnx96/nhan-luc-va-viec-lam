<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use App\Models\Career;
use App\Models\Human;
use App\Models\Job;
use App\Models\JobTraining;
use App\Models\JobOfficer;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home.index');
    }
}
