<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function getLogin(Request $request){
        return view('backend.account.login.index');
    }

    public function postLogin(Request $request){
        $data = $request->only('email', 'password');
        if (Auth::attempt($data)) {
            return redirect()->route('admin.home.index')->with('success', 'Đăng nhập hệ thống thành công');
        }

        return back()->with('fail', 'Email hoặc mật khẩu không đúng !');
    }
}
