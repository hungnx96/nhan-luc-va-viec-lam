<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index($slug)
    {
        $post = Post::where('slug', $slug)->first();
        return view('frontend.home.post', [
            'post' => $post
        ]);
    }

    public function tinTuyenSinh($id)
    {
        $post = Post::findOrFail($id);
        return view('frontend.home.tintuyensinh', [
            'post' => $post
        ]);
    }
}
