<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Career;
use App\Models\Category;
use Illuminate\Http\Request;

class EducateController extends Controller
{
    public function index(Request $request)
    {
        $catId = $request->cat_id;
        $DetailId = $request->detail_id;
        $post = Post::where('category_id', $DetailId)->first();
        $careers = Career::where('category_id', $DetailId)->get();
        $categories = Category::where('id', $catId)
                            ->with('childrenCategories')
                            ->first();
        $childrenCategories = $categories->childrenCategories;

        $arr_b = array(
                  0 =>  array(
                        'name' => 'Trang chủ',
                        'href'  => '/'
                    )
        );

        $breadcrumbs = array_merge($this->getBreadcrumbsCategory($catId), $arr_b);
        krsort($breadcrumbs);
        if($childrenCategories->isEmpty()){
            $keys = array_slice($breadcrumbs, -3, 1);
            $categoryId = $keys[0]['id'] ?? null;
            $category = Category::where('id', $categoryId)
                        ->with('childrenCategories')
                        ->first();

            $childrenCategories = $category->childrenCategories;
        }
        return view('frontend.home.educate', [
            'categories' => $categories,
            'post' => $post,
            'careers' => $careers,
            'breadcrumbs' => $breadcrumbs,
            'childrenCategories' => $childrenCategories,
        ]);
    }

    public function educate1()
    {
        return view('frontend.home.educate');
    }

    private function getBreadcrumbsCategory($idCate, $data = array()) {
        $category = Category::find($idCate);
        $link = 'dao-tao?cat_id='.$idCate;
        $data[]           = array(
            'name' => (isset($category->name) ? $category->name : ''),
            'href'  => $link,
            'id'  => $idCate,
        );
        if (isset($category->category_id) && $category->category_id > 0) {
            $data = $this->getBreadcrumbsCategory($category->category_id, $data);
        }

        return $data;
    }
}
