<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Post;
use App\Models\Category;
use App\Models\JobOfficer;
use App\Models\JobTraining;
use App\Models\JobStudent;
use App\Models\JobShort;
use App\Models\JobExport;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function index(Request $request)
    {
        $catId = $request->cat_id;
        $DetailId = $request->detail_id;
        $post = Post::where('category_id', $DetailId)->first();
        $jobs = Job::where('category_id', $DetailId)->get();
        $jobTrainings = JobTraining::where('category_id', $DetailId)->get();
        $JobOfficers = JobOfficer::where('category_id', $DetailId)->get();
        $JobStudents = JobStudent::where('category_id', $DetailId)->get();
        $JobShorts = JobShort::where('category_id', $DetailId)->get();
        $JobExports = JobExport::where('category_id', $DetailId)->get();
        $categories = Category::where('id', $catId)
            ->with('childrenCategories')
            ->first();
        $childrenCategories = $categories->childrenCategories;
        $arr_b = array(
            0 =>  array(
                'name' => 'Trang chủ',
                'href'  => '/'
            )
        );

        $breadcrumbs = array_merge($this->getBreadcrumbsCategory($catId), $arr_b);
        krsort($breadcrumbs);

        if($childrenCategories->isEmpty()){
            $keys = array_slice($breadcrumbs, -3, 1);
            $categoryId = $keys[0]['id'] ?? null;
            $category = Category::where('id', $categoryId)
                        ->with('childrenCategories')
                        ->first();
            $childrenCategories = $category->childrenCategories;
        }

        return view('frontend.home.job', [
            'categories' => $categories,
            'post' => $post,
            'jobTrainings' => $jobTrainings,
            'JobOfficers' => $JobOfficers,
            'JobStudents' => $JobStudents,
            'JobShorts' => $JobShorts,
            'JobExports' => $JobExports,
            'jobs' => $jobs,
            'breadcrumbs' => $breadcrumbs,
            'childrenCategories' => $childrenCategories,
        ]);
    }

    private function getBreadcrumbsCategory($idCate, $data = array()) {
        $category = Category::find($idCate);
        $link = 'viec-lam?cat_id='.$idCate;
        $data[]           = array(
            'name' => (isset($category->name) ? $category->name : ''),
            'href'  => $link,
            'id'  => $idCate,
        );
        if (isset($category->category_id) && $category->category_id > 0) {
            $data = $this->getBreadcrumbsCategory($category->category_id, $data);
        }

        return $data;
    }
}
