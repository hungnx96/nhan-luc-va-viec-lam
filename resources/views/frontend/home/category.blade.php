@extends('frontend.layouts.app')

@section('content')
 <div class="col-md-12">
    <div class="breadcrumb">
        <ul>
            <li><a href="">Trang chủ</a></li>
            <li><a href="">Đạo tạo nhân lực</a></li>
            <li><a href="">Đạo tạo đại học</a></li>
        </ul>
    </div>
    </div>
    <div class="row">
    <div class="col-md-3 right-padding5">
    <div class="course-widget-price course-category">
   <div class="course-category-title">
      <h4> Danh mục </h4>
        </div>
        <div class="list-category">
            <ul>
                @foreach ($categories as $category)
                    <li>{{ $category->name }}</li>
                    <ul>
                        @foreach ($category->childrenCategories as $childCategory)
                            @include('frontend.home.child_category', ['child_category' => $childCategory, 'hasIcon' => true])
                        @endforeach
                    </ul>
                @endforeach
            </ul>
        </div>
        </div>
    </div>
    <div class="col-md-9 left-padding5">
        <div class="info-item">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active show" href="#" role="tab" data-toggle="tab" aria-selected="false">
                    {{{ $post->name }}}
                </a>
            </li>
        </ul>
        <div role="tabpanel" class="tab-pane fade active show" id="successful">
            <div class="blog-detail-text">
                    @if(!empty($post->body))
                      <p>{{{ $post->body }}}</p>
                    @endif
                    @if($careers->count() > 0)
                        <table class="table table-bordered">
                            <thead style="background: #0F3661;
                                            color: #fff;
                                            text-transform: uppercase;">
                                <tr>
                                    <td rowspan="2" style="
                                        width: 19%;
                                    ">Tên ngành đào tạo</td>
                                    <td colspan="2">Chỉ tiêu tuyển sinh</td>
                                    <td rowspan="2" style="
                                        width: 16%;
                                    ">Thời gian tuyển sinh</td>
                                    <td rowspan="2" style="
                                        width: 23%;
                                    ">Hình thức, hồ sơ, đăng ký dự tuyển sinh</td>
                                    <td rowspan="2">Tư vấn, hỗ trợ</td>
                                </tr>
                                <tr>
                                    <td>2020</td>
                                    <td>2021</td>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($careers as $career)
                                    <tr>
                                    <td>{{ $career->name }}</td>
                                    <td>{{ $career->target_1 }}</td>
                                    <td>{{ $career->target_2 }}</td>
                                    <td>{{ $career->date }}</td>
                                    <td>
                                        <a href="{{route('news', 16)}}" class="btn btn-primary btn-sm">Chi tiết</a>
                                    </td>
                                    <td>
                                        <img src="{{ url('images/phone.png') }}" alt="" width="30px">
                                        <!-- <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                                            1111
                                            </button> -->
                                            <!-- <a href="#" data-toggle="tooltip" title="Hooray!">Hover over me</a> -->
                                        <img src="{{ url('images/email.png') }}" alt="" width="40px">
                                    </td>
                                    </tr>
                                @endforeach

                            </tbody>
                            </table>
                        @endif
            </div>
        </div>
    </div>
    </div>
    </div>

@endsection
