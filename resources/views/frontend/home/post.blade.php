@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="">Bài viết</a></li>
            </ol>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="blog-detail-content pb40" style="/* margin-top:50px; */background: #fff;padding: 15px;border-radius: 15px;">
                    <h3>{{ $post->name }}</h3>
                    <div class="blog-detail-meta">
                        <ul class="meta">
                            <li>16/04/2020 08:20:42 </li>
                        </ul>
                    </div>
                    <div class="blog-detail-pic pb30">
                        <p>{{ $post->body }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
