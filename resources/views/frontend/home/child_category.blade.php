<?php
   $name = $child_category->name;
   $id   = $child_category->id;
   $checkHasIcon = ($hasIcon && $countIcon == 0);
?>
<li @if($id == request('detail_id') || $id == request('cat_id')) class="active-green"  @endif>
	@if($countIcon == 0)
		@if($child_category->categories->isNotEmpty())
			<a href="javascript::void(0)" >
				@if($checkHasIcon)<span class="icon-f"></span>@endif {{ $name }}
			</a>
		@else
			<a href="{{ URL::current() }}?cat_id={{ $id }}&detail_id={{ $id }}" >
				@if($checkHasIcon)<span class="icon-f"></span>@endif {{ $name }}
			</a>
		@endif
	@else
		@if($child_category->categories->isNotEmpty())
			<a href="javascript::void(0)" >
				@if($checkHasIcon)<span class="icon-f"></span>@endif {{ $name }}
			</a>
		@else
			<a href="{{ URL::current() }}?cat_id={{ $id }}&detail_id={{ $id }}" >
				@if($checkHasIcon)<span class="icon-f"></span>@endif {{ $name }}
			</a>
		@endif
	@endif
</li>
@if ($child_category->categories)
<ul>
   @foreach ($child_category->categories as $childCategory)
   <?php
      $hasIcon = $childCategory->categories->isNotEmpty() ? true : false;
      $countIcon =+ 1;
    ?>
   @if ($hasIcon && $countIcon == 1)
	<i class="fas fa-chevron-right"  style="
		float: left;
		margin-top: 11px;
		font-size: 9px;
		margin-left: -5px;
		color: #004370;
		"></i>
   @endif
   @include('frontend.home.child_category', ['child_category' => $childCategory, 'hasIcon' => $hasIcon, 'countIcon' => $countIcon, 'showLink' => true])
   <?php
      $countIcon = 0;
    ?>
   @endforeach
</ul>
@endif