@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="#">Tin tuyển sinh</a></li>
            </ol>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="blog-detail-content pb40" style="margin-top:50px">
                    <h3>{{ $post->name }}</h3>
                    <div class="blog-detail-meta pb30">
                        <ul class="meta">
                            <li>16/04/2020 08:20:42 </li>
                        </ul>
                    </div>
                    <div class="blog-detail-pic pb30">
                        <p>{{ $post->body }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
