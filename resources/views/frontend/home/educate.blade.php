@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                @foreach ($breadcrumbs as $breadcrumb)
                    <li class="breadcrumb-item"><a href="{{ url($breadcrumb['href']) }}">{{ $breadcrumb['name'] }}</a></li>
                @endforeach
            </ol>
        </nav>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-3 right-padding5">
                <div class="course-widget-price course-category">
                    <div class="course-category-title">
                        <h4> Danh mục</h4>
                    </div>
                    <div class="list-category">
                        <ul>
                            <?php
                            $countArr = count($breadcrumbs);
                            end($breadcrumbs);
                            $backToLink = prev($breadcrumbs);
                            ?>
                            <li id="cate_name">{{ $categories->name }}</li>
                            <ul>
                                @foreach ($childrenCategories as $childCategory)
                                    @include('frontend.home.child_category', ['child_category' => $childCategory, 'hasIcon' => true, $countIcon = 0, 'showLink' => false])
                                @endforeach
                            </ul>
                        </ul>
                         <!-- new menu -->
                        {{-- <ul>
                            <li id="cate_name">Đào tạo đại học</li>
                            <ul class="sub-menu1">
                                <li>
                                    <a href="http://localhost/nhan-luc-va-viec-lam/public/dao-tao?cat_id=4&amp;detail_id=5">
                                        <i class="fa fa-book"></i>
                                       Đào tạo đại học theo chương trình chuẩn
                                    </a>
                                </li>
                                <ul class="sub-menu2">
                                    <span class="ava-123"></span>
                                    <li>
                                        <a href="http://localhost/nhan-luc-va-viec-lam/public/dao-tao?cat_id=4&amp;detail_id=6">
                                            Hệ đại học chính quy
                                        </a>
                                    </li>
                                    <ul class="sub-menu3">
                                        <span class="ava-123"></span>
                                        <li>
                                            <a href="http://localhost/nhan-luc-va-viec-lam/public/dao-tao?cat_id=4&amp;detail_id=8">
                                               Ngành/chuyên nghành đào tạo
                                            </a>
                                        </li>
                                        <ul>
                                        </ul>
                                    </ul>
                                    <span class="ava-123"></span>
                                    <li>
                                        <a href="http://localhost/nhan-luc-va-viec-lam/public/dao-tao?cat_id=4&amp;detail_id=9">
                                            Hệ đại học vừa làm vừa học
                                        </a>
                                    </li>
                                    <ul class="sub-menu3">
                                        <span class="ava-123"></span>
                                        <li>
                                            <a href="http://localhost/nhan-luc-va-viec-lam/public/dao-tao?cat_id=4&amp;detail_id=11">
                                               Ngành/chuyên nghành đào tạo
                                            </a>
                                        </li>
                                        <ul>
                                        </ul>
                                    </ul>
                                </ul>
                            </ul>
                        </ul> --}}
                    </div>
                </div>
            </div>
            <div class="col-md-9 left-padding5">
                <div class="info-item">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" href="#" role="tab" data-toggle="tab" aria-selected="false">
                                {{{ $post->name ?? null }}}
                            </a>
                        </li>
                    </ul>
                    <div role="tabpanel" class="tab-pane fade active show" id="successful">
                        <div class="blog-detail-text">
                            @if(!empty($post->body))
                                <p>{{{ $post->body }}}</p>
                            @endif
                            @if($careers->count() > 0)
                                <table class="table table-bordered">
                                    <thead style="background: #0F3661;
                                            color: #fff;">
                                    <tr>
                                        <td class="text-center">STT</td>
                                        <td class="text-center">Tên ngành đào tạo</td>
                                        <td style="text-align: center;
                                    ">Thời gian tuyển sinh</td>
                                        <td class="text-center" data-toggle="tooltip" data-placement="top" title="Hình thức, hồ sơ, đăng ký dự tuyển sinh">
                                            Hình thức, hồ sơ...
                                        </td>
                                        <td style="text-align: center"  data-toggle="tooltip" data-placement="top" title="Tư vấn, hỗ trợ đăng ký dự tuyển sinh">Tư vấn, hỗ trợ...</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($careers as $key => $career)
                                        <tr>
                                            <td class="text-center">{{ $key + 1 }}</td>
                                            <td>{{ $career->name }}</td>
                                            <td style="text-align: center">{{ $career->date }}</td>
                                            <td style="text-align: center">
                                                <a href="{{route('news', 16)}}" class="btn btn-primary btn-sm">Chi tiết</a>
                                            </td>
                                            <td  style="text-align: center">
                                                <img data-toggle="tooltip" data-placement="top" title="0983.054.815" src="{{ url('images/phone.png') }}" alt="" width="20px">
                                                <img data-toggle="tooltip" data-placement="top" title="ttdtcbcc@gmail.com" src="{{ url('images/email.png') }}" alt="" width="20px">
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            var mvalue = $("#cate_name").text();
            $( "#cate_name_p" ).html(mvalue );
        });
    </script>
@endsection
