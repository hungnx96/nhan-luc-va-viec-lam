@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                @foreach ($breadcrumbs as $breadcrumb)
                    <li class="breadcrumb-item"><a href="{{ url($breadcrumb['href']) }}">{{ $breadcrumb['name'] }}</a></li>
                @endforeach
            </ol>
        </nav>
    </div>
   <div class="container">
    <div class="row">
        <div class="col-md-3 right-padding5">
        <div class="course-widget-price course-category">
       <div class="course-category-title">
          <h4> Danh mục</h4>
            </div>
            <div class="list-category">
                <ul>
                    <?php
                    $countArr = count($breadcrumbs);
                    end($breadcrumbs);
                    $backToLink = prev($breadcrumbs);
                    ?>
                    <li id="cate_name">{{ $categories->name }}</li>
                    <ul>
                        @foreach ($childrenCategories as $childCategory)
                            @include('frontend.home.child_category', ['child_category' => $childCategory, 'hasIcon' => true, $countIcon = 0])
                        @endforeach
                    </ul>
                </ul>
            </div>
            </div>
        </div>
        <div class="col-md-9 left-padding5">
            <div class="info-item">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" href="#" role="tab" data-toggle="tab" aria-selected="false">
                        {{{ $post->name }}}
                    </a>
                </li>
            </ul>
            <div role="tabpanel" class="tab-pane fade active show" id="successful">
                <div class="blog-detail-text">
                        @if(!empty($post->body))
                          <p>{{{ $post->body }}}</p>
                        @endif
                        @if($jobs->count() > 0)
                            <table class="table table-bordered">
                                <thead style="background: #0F3661;
                                                color: #fff;">
                                    <tr>
                                        <td>Mã đề tài </td>
                                        <td>Thành viên, nội dung, địa điểm nghiên cứu</td>
                                        <td>Yêu cầu về người tham gia nghiên cứu</td>
                                        <td>Điều kiện thực hiện công việc</td>
                                        <td>Tư vấn, hỗ trợ</td>
                                    </tr>
                                </thead>
                                <tbody>
                                   @foreach($jobs as $job)
                                        <tr>
                                            <td>{{ $job->name }}</td>
                                            <td class="tp-0">{!! $job->level !!}</td>
                                            <td class="tp-0">{!! $job->field !!}</td>
                                            <td class="text-center">
                                                <a href="{{route('news', 16)}}" class="btn btn-primary btn-sm">Chi tiết</a>
                                            </td>
                                            <td  class="text-center">
                                                <img data-toggle="tooltip" data-placement="top" title="0983.054.815" src="{{ url('images/phone.png') }}" alt="" width="20px">
                                                <img data-toggle="tooltip" data-placement="top" title="ttdtcbcc@gmail.com" src="{{ url('images/email.png') }}" alt="" width="20px">
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                                </table>
                                <nav aria-label="Page navigation example" class="sub-navigation">
                                    <ul class="pagination">
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            @endif

                            @if($jobTrainings->count() > 0)
                                <table class="table table-bordered">
                                    <thead style="background: #0F3661;
                                                color: #fff;">
                                    <tr>
                                        <td class="text-center">STT</td>
                                        <td class="text-center">Nội dung đào tạo, bồi dưỡng</td>
                                        <td  class="text-center" data-toggle="tooltip" data-placement="top" title="Yêu cầu về giảng viên, báo cáo viên">Yêu cầu...</td>
                                        <td class="text-center">Thời gian</td>
                                        <td class="text-center">Thời gian, địa điểm giảng dạy</td>
                                        <td  class="text-center" data-toggle="tooltip" data-placement="top" title="Điều kiện thực hiện công việc">Điều kiện...</td>
                                        <td  class="text-center">Tư vấn, hỗ trợ</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($jobTrainings as $key => $jobTraining)
                                        <tr>
                                            <td class="text-center">{{ $key + 1 }}</td>
                                            <td class="text-center">{{ $jobTraining->content }}</td>
                                            <td class="text-center">{{ $jobTraining->require }}</td>
                                            <td class="text-center">{{ $jobTraining->date }}</td>
                                            <td class="text-center">{{ $jobTraining->local }}</td>
                                            <td class="text-center">
                                                <a href="{{route('news', 16)}}" class="btn btn-primary btn-sm">Chi tiết</a>
                                            </td>
                                            <td style="text-align: center">
                                                <img data-toggle="tooltip" data-placement="top" title="0983.054.815" src="{{ url('images/phone.png') }}" alt="" width="20px">
                                                <img data-toggle="tooltip" data-placement="top" title="ttdtcbcc@gmail.com" src="{{ url('images/email.png') }}" alt="" width="20px">
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            @endif

                            @if($JobOfficers->count() > 0)
                                <table class="table table-bordered">
                                    <thead style="background: #0F3661;
                                                color: #fff;">
                                    <tr>
                                        <td>Mã đơn vị tuyển dụng</td>
                                        <td class="text-center">Thông tin tuyển dụng</td>
                                        <td class="text-center">Địa điểm làm việc</td>
                                        <td class="text-center">Chế độ tiền lương, bảo hiểm</td>
                                        <td class="text-center">Tư vấn, hỗ trợ</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($JobOfficers as $JobOfficer)
                                        <tr>
                                            <td>{{ $JobOfficer->company }}</td>
                                            <td class="text-center">
                                                <a href="{{route('news', 16)}}" class="btn btn-primary btn-sm">Chi tiết</a>
                                            </td>
                                            <td>{{ $JobOfficer->local }}</td>
                                            <td class="text-center">{{ $JobOfficer->regime }}</td>
                                            <td  style="text-align: center">
                                                <img data-toggle="tooltip" data-placement="top" title="0983.054.815" src="{{ url('images/phone.png') }}" alt="" width="20px">
                                                <img data-toggle="tooltip" data-placement="top" title="ttdtcbcc@gmail.com" src="{{ url('images/email.png') }}" alt="" width="20px">
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            @endif
                            @if($JobStudents->count() > 0)
                                <table class="table table-bordered">
                                    <thead style="background: #0F3661;
                                                color: #fff;">
                                    <tr>
                                        <td>Mã đơn vị tuyển sinh</td>
                                        <td class="text-center">Hồ sơ pháp lý</td>
                                        <td class="text-center">Thông tin tuyển sinh</td>
                                        <td class="text-center">Tư vấn, hỗ trợ</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($JobStudents as $JobStudent)
                                        <tr>
                                            <td>{{ $JobStudent->code }}</td>
                                            <td class="text-center">
                                                <a href="{{route('news', 16)}}" class="btn btn-primary btn-sm">Chi tiết</a>
                                            </td>
                                            <td class="text-center"><a href="{{route('news', 16)}}" class="btn btn-primary btn-sm">Chi tiết</a></td>
                                            <td  style="text-align: center">
                                                <img data-toggle="tooltip" data-placement="top" title="0983.054.815" src="{{ url('images/phone.png') }}" alt="" width="20px">
                                                <img data-toggle="tooltip" data-placement="top" title="ttdtcbcc@gmail.com" src="{{ url('images/email.png') }}" alt="" width="20px">
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            @endif
                            @if($JobShorts->count() > 0)
                                <table class="table table-bordered">
                                    <thead style="background: #0F3661;
                                                color: #fff;">
                                    <tr>
                                        <td>Mã đơn vị tuyển dụng</td>
                                        <td class="text-center">Thông tin tuyển dụng</td>
                                        <td class="text-center">Địa điểm làm việc</td>
                                        <td class="text-center">Chế độ tiền lương, bảo hiểm</td>
                                        <td class="text-center">Tư vấn, hỗ trợ</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($JobShorts as $JobShort)
                                        <tr>
                                            <td>{{ $JobShort->code }}</td>
                                            <td class="text-center">
                                                <a href="{{route('news', 16)}}" class="btn btn-primary btn-sm">Chi tiết</a>
                                            </td>
                                            <td class="text-center">{{ $JobShort->local }}</td>
                                            <td class="text-center">{{ $JobShort->condition }}</td>
                                            <td  style="text-align: center">
                                                <img data-toggle="tooltip" data-placement="top" title="0983.054.815" src="{{ url('images/phone.png') }}" alt="" width="20px">
                                                <img data-toggle="tooltip" data-placement="top" title="ttdtcbcc@gmail.com" src="{{ url('images/email.png') }}" alt="" width="20px">
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            @endif
                            @if($JobExports->count() > 0)
                                <table class="table table-bordered">
                                    <thead style="background: #0F3661;
                                                color: #fff;">
                                    <tr>
                                        <td>Mã đơn vị tuyển dụng</td>
                                        <td class="text-center">Hồ sơ pháp lý</td>
                                        <td class="text-center">Thông tin tuyển dụng</td>
                                        <td class="text-center">Tư vấn, hỗ trợ</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($JobExports as $JobExport)
                                        <tr>
                                            <td>{{ $JobExport->code }}</td>
                                            <td class="text-center">
                                                <a href="{{route('news', 16)}}" class="btn btn-primary btn-sm">Chi tiết</a>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{route('news', 16)}}" class="btn btn-primary btn-sm">Chi tiết</a>
                                            </td>
                                            <td  style="text-align: center">
                                                <img data-toggle="tooltip" data-placement="top" title="0983.054.815" src="{{ url('images/phone.png') }}" alt="" width="20px">
                                                <img data-toggle="tooltip" data-placement="top" title="ttdtcbcc@gmail.com" src="{{ url('images/email.png') }}" alt="" width="20px">
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            @endif

                </div>
            </div>
        </div>
    </div>
    </div>
       </div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            var mvalue = $("#cate_name").text();
            $( "#cate_name_p" ).html(mvalue );
        });
    </script>
@endsection
