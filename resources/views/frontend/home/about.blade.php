@extends('frontend.layouts.app')

@section('content')
    <div class="col-md-12">
    <div class="breadcrumb">
        <ul>
            <li><a href="">Trang chủ</a></li>
            <li><a href="">Đạo tạo nhân lực</a></li>
            <li><a href="{{route('human', 'dao-tao-thac-sy')}}">Đạo tạo thạc sỹ</a></li>
            <li><a href="" style="color:#868686">Giới thiệu</a></li>
        </ul>
    </div>
    </div>
    <div class="row">
    <div class="col-md-3 right-padding5">
    <div class="course-widget-price course-category">
   <div class="course-category-title">
      <h4> Danh mục</h4>
        </div>
        <div class="list-category">
            <ul>
                <li>
                    <a class="category-course-active cate-item " href="{{ url('/nhan-luc/chi-tiet/gioi-thieu') }}">
                       Giới thiệu
                    </a>
                </li>
                <li>
                    <a class="cate-item " href="{{ url('nhan-luc/chi-tiet/nganh-dao-tao')}}">
                       Ngành/chuyên nghành đào tạo
                    </a>
                </li>
            </ul>
        </div>
        </div>
    </div>
    <div class="col-md-9 left-padding5">
        <div class="info-item">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"> <a class="nav-link active show" href="#successful" role="tab" data-toggle="tab" aria-selected="false">Giới thiệu</a> </li>
        </ul>
        <div role="tabpanel" class="tab-pane fade active show" id="successful">
            <div class="blog-detail-text">
                    <p>Nội dung...</p>
            </div>
        </div>
    </div>
    </div>
    </div>

@endsection
