@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3 right-padding5">
                <div class="menu-left">
                    <ul class="mar-bottom-5">
                        <li>
                            <p class="menu-left-title">ĐÀO TẠO NHÂN LỰC</p>
                            <ul>
                                <li><a href="{{ url("dao-tao?cat_id=1&detail_id=2") }}">Đào tạo thạc sỹ</a></li>
                                <li><a href="{{ url("dao-tao?cat_id=4&detail_id=2") }}">Đào tạo đại học</a></li>
                                <li><a href="{{ url("dao-tao?cat_id=18&detail_id=2") }}">Đào tạo, bồi dưỡng cán bộ, công chức, viên chức</a></li>
                                <li><a href="{{ url("dao-tao?cat_id=21&detail_id=2") }}">Đào tạo tin học</a></li>
                                <li><a href="{{ url("dao-tao?cat_id=24&detail_id=2") }}">Đào tạo ngoại ngữ</a></li>
                                <li><a href="{{ url("dao-tao?cat_id=27&detail_id=2") }}">Đào tạo hướng dẫn viên du lịch nội địa</a></li>
                                <li><a href="{{ url("dao-tao?cat_id=30&detail_id=2") }}">Đào tạo hướng dẫn viên du lịch quốc tế</a></li>
                                <li><a href="{{ url("dao-tao?cat_id=33&detail_id=2") }}">Bồi dưỡng, cấp chứng chỉ kế toán trưởng</a></li>
                                <li><a href="{{ url("dao-tao?cat_id=36&detail_id=2") }}">Bồi dưỡng, cấp chứng chỉ kế toán viên</a></li>
                            </ul>
                        </li>
                        <li>
                            <p class="menu-left-title" style="margin-top: 12px">CUNG ỨNG NHÂN LỰC</p>
                            <ul>
                                <li><a href="{{ url("nhan-luc?cat_id=39&detail_id=2") }}">Chuyên gia</a></li>
                                <li><a href="{{ url("nhan-luc?cat_id=42&detail_id=2") }}">Giảng viên, báo cáo viên</a></li>
                                <li><a href="{{ url("nhan-luc?cat_id=54&detail_id=2") }}">Sinh viên tốt nghiệp đại học</a></li>
                                <li><a href="{{ url("nhan-luc?cat_id=57&detail_id=2") }}">Nhân lực làm việc ngắn hạn </a></li>
                            </ul>
                        </li>
                        <li>
                            <p class="menu-left-title" style="margin-top: 12px">CUNG ỨNG VIỆC LÀM</p>
                            <ul>
                                <li><a href="{{ url("viec-lam?cat_id=61&detail_id=2") }}">Nghiên cứu đề tài khoa học</a> </li>
                                <li><a href="{{ url("viec-lam?cat_id=64&detail_id=2") }}">Giảng dạy chương trình đào tạo, bồi dưỡng</a></li>
                                <li><a href="{{ url("viec-lam?cat_id=67&detail_id=2") }}">Tuyển công chức, viên chức</a></li>
                                <li><a href="{{ url("viec-lam?cat_id=70&detail_id=2") }}">Tuyển nhân lực theo vị trí việc làm trong doanh nghiệp</a></li>
                                <li><a href="{{ url("viec-lam?cat_id=73&detail_id=2") }}">Tuyển sinh du học</a></li>
                                <li><a href="{{ url("viec-lam?cat_id=76&detail_id=2") }}">Tuyển thực tập sinh</a></li>
                                <li><a href="{{ url("viec-lam?cat_id=79&detail_id=2") }}">Tuyển nhân lực xuất khẩu lao động</a></li>
                                <li><a href="{{ url("nhan-luc?cat_id=82&detail_id=2") }}">Tuyển nhân lực làm việc ngắn hạn  </a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 left-padding5 right-padding5">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <a href="https://www.google.com/" target="_blank">
                                <img src="images/banner/banner_1.jpg" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Triển lãm Thành tựu khoa học ứng dụng khí tượng thủy văn trong dự báo, cảnh báo phục vụ phát triển kinh tế - xã hội</h5>
                                </div>
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="https://www.google.com/" target="_blank">
                                <img src="images/banner/banner_2.jpg" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>HUNRE tặng quà cho thí sinh nhập học năm 2020</h5>
                                </div>
                            </a>
                        </div>
                        <div class="carousel-item">
                            <a href="https://www.google.com/" target="_blank">
                                <img src="images/banner/banner_3.jpg" class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Triển lãm Thành tựu khoa học ứng dụng khí tượng thủy văn trong dự báo, cảnh báo phục vụ phát triển kinh tế - xã hội</h5>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="home-banner-s">
                    <a href="{{ url('/bai-viet/quang-cao') }}"><img src="images/banner/banner_4.jpg" alt=""></a>
                </div>
            </div>
            <div class="col-md-3 left-padding5">
                <div class="home-ads">
                    <a href="{{ url('/bai-viet/quang-cao') }}"><img src="images/ads/7.png?v=2.3" alt=""></a>
                    <a href="{{ url('/bai-viet/quang-cao') }}"><img src="images/ads/8.png?v=2.3" alt=""></a>
                    <a href="{{ url('/bai-viet/quang-cao') }}"><img src="images/ads/9.png?v=2.3" alt=""></a>
                </div>
            </div>
        </div>
    </div>
@endsection
