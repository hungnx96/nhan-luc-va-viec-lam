@extends('frontend.layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                @foreach ($breadcrumbs as $breadcrumb)
                    <li class="breadcrumb-item"><a href="{{ url($breadcrumb['href']) }}">{{ $breadcrumb['name'] }}</a></li>
                @endforeach
            </ol>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 right-padding5">
                <div class="course-widget-price course-category">
                    <div class="course-category-title">
                        <h4> Danh mục</h4>
                    </div>
                    <div class="list-category">
                        <ul>
                            <?php
                            $countArr = count($breadcrumbs);
                            end($breadcrumbs);
                            $backToLink = prev($breadcrumbs);
                            ?>
                            <li id="cate_name">{{ $categories->name }}</li>
                            <ul>
                                @foreach ($childrenCategories as $childCategory)
                                    @include('frontend.home.child_category', ['child_category' => $childCategory, 'hasIcon' => true, $countIcon = 0])
                                @endforeach
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9 left-padding5">
                <div class="info-item">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" href="#" role="tab" data-toggle="tab" aria-selected="false">
                                {{{ $post->name }}}
                            </a>
                        </li>
                    </ul>
                    <div role="tabpanel" class="tab-pane fade active show" id="successful">
                        <div class="blog-detail-text">
                            @if(!empty($post->body))
                                <p>{{{ $post->body }}}</p>
                            @endif
                            @if($humans->count() > 0)
                                <table class="table table-bordered">
                                    <thead style="background: #0F3661;
                                            color: #fff;">
                                    <tr>
                                        <td>Mã nhân lực </td>
                                        <td>Năng lực, công việc đảm nhận</td>
                                        <td class="text-center">Thông tin nhân lực</td>
                                        <td class="text-center">Tư vấn, hỗ trợ</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($humans as $human)
                                        <tr>
                                            <td>{{ $human->code }}</td>
                                            <td>{{ $human->position }}</td>
                                            <td style="text-align: center">
                                                <a href="{{route('news', 16)}}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">Chi tiết</a>
                                            </td>
                                            <td  style="text-align: center">
                                                <img data-toggle="tooltip" data-placement="top" title="0983.054.815" src="{{ url('images/phone.png') }}" alt="" width="20px">
                                                <img data-toggle="tooltip" data-placement="top" title="ttdtcbcc@gmail.com" src="{{ url('images/email.png') }}" alt="" width="20px">
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                                    <nav aria-label="Page navigation example" class="sub-navigation">
                                        <ul class="pagination">
                                            <li class="page-item">
                                                <a class="page-link" href="#" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item">
                                                <a class="page-link" href="#" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                            @endif

                            @if($humanLecturers->count() > 0)
                                <table class="table table-bordered">
                                    <thead style="background: #0F3661;
                                        color: #fff;">
                                    <tr>
                                        <td>Mã nhân lực </td>
                                        <td>Học hàm, học vị, chức vụ</td>
                                        <td>Năng lực, công việc đảm nhận</td>
                                        <td class="text-center">Thông tin nhân lực</td>
                                        <td class="text-center">Tư vấn, hỗ trợ</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($humanLecturers as $humanLecturer)
                                        <tr>
                                            <td>{{ $humanLecturer->code }}</td>
                                            <td class="text-center">{{ $humanLecturer->level }}</td>
                                            <td class="text-center">{{ $humanLecturer->position }}</td>
                                            <td style="text-align: center">
                                                <a href="{{route('news', 16)}}" class="btn btn-primary btn-sm">Chi tiết</a>
                                            </td>
                                            <td  style="text-align: center">
                                                <img data-toggle="tooltip" data-placement="top" title="0983.054.815" src="{{ url('images/phone.png') }}" alt="" width="20px">
                                                <img data-toggle="tooltip" data-placement="top" title="ttdtcbcc@gmail.com" src="{{ url('images/email.png') }}" alt="" width="20px">
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            var mvalue = $("#cate_name").text();
            $( "#cate_name_p" ).html(mvalue );
        });
    </script>
@endsection
