@extends('frontend.layouts.app')
@section('content')
<form action="#" method="post" id="register-form">
    <h2>ĐĂNG KÝ</h2>
	<p>
		<label for="Name" class="floatLabel">Tên</label>
		<input id="Name" name="Name" type="text">
	</p>
	<p>
		<label for="address" class="floatLabel">Địa chỉ</label>
		<input id="address" name="address" type="text">
	</p>
	<p>
		<label for="phone_number" class="floatLabel">Số điện thoại</label>
		<input id="phone_number" name="phone_number" type="text">
	</p>
	<p>
		<input type="submit" value="Đăng ký" id="submit">
	</p>
</form>
@endsection
