<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css?v=') }}{{ time() }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css?v=') }}{{ time() }}" rel="stylesheet">
    <link href="{{ asset('css/common.css?v=') }}{{ time() }}" rel="stylesheet">
    <link href="{{ asset('css/navigation.css?v=') }}{{ time() }}" rel="stylesheet">
    <link href="{{ asset('css/sidebar/sidebar.css?v=') }}{{ time() }}" rel="stylesheet">
    <title>Nhân lực và việc làm</title>
</head>
<body>
<div class="container">
    <div class="home-banner-top">
        <a href="{{ url('/') }}"><img src="{{ asset('images/banner_8.jpg?v=1.3') }}" alt=""></a>
    </div>
    <nav class="navbar navbar-expand-lg">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="parent-menu">
                <div class="top-menu-left">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}"><i class="fa fa-home"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/bai-viet/gioi-thieu') }}">Giới thiệu</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-icon" href="{{ url('/dao-tao?cat_id=85') }}" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Đào tạo nhân lực
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url("dao-tao?cat_id=1&detail_id=2") }}">Đào tạo thạc sỹ</a>
                                <a class="dropdown-item" href="{{ url("dao-tao?cat_id=4&detail_id=2") }}">Đào tạo đại học</a>
                                <a class="dropdown-item" href="{{ url("dao-tao?cat_id=18&detail_id=2") }}">Đào tạo, bồi dưỡng cán bộ, công chức...</a>
                                <a class="dropdown-item" href="{{ url("dao-tao?cat_id=21&detail_id=2") }}">Đào tạo tin học</a>
                                <a class="dropdown-item" href="{{ url("dao-tao?cat_id=24&detail_id=2") }}">Đào tạo ngoại ngữ</a>
                                <a class="dropdown-item" href="{{ url("dao-tao?cat_id=27&detail_id=2") }}">Đào tạo hướng dẫn viên du lịch nội địa</a>
                                <a class="dropdown-item" href="{{ url("dao-tao?cat_id=30&detail_id=2") }}">Đào tạo hướng dẫn viên du lịch quốc tế</a>
                                <a class="dropdown-item" href="{{ url("dao-tao?cat_id=33&detail_id=2") }}">Bồi dưỡng, cấp chứng chỉ kế toán trưởng</a>
                                <a class="dropdown-item" href="{{ url("dao-tao?cat_id=36&detail_id=2") }}">Bồi dưỡng, cấp chứng chỉ kế toán viên</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-icon" href="{{ url('/dao-tao?cat_id=86') }}" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Cung ứng nhân lực
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url("nhan-luc?cat_id=39&detail_id=39") }}">Chuyên gia</a>
                                <a class="dropdown-item" href="{{ url("nhan-luc?cat_id=42&detail_id=42") }}">Giảng viên, báo cáo viên</a>
                                <a class="dropdown-item" href="{{ url("nhan-luc?cat_id=54&detail_id=54") }}">Sinh viên tốt nghiệp đại học</a>
                                <a class="dropdown-item" href="{{ url("nhan-luc?cat_id=57&detail_id=57") }}">Nhân lực làm việc ngắn hạn</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-icon" href="{{ url('/dao-tao?cat_id=87') }}" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Cung ứng việc làm
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url("viec-lam?cat_id=61&detail_id=61") }}">Nghiên cứu đề tài khoa học</a>
                                <a class="dropdown-item" href="{{ url("viec-lam?cat_id=64&detail_id=64") }}">Giảng dạy chương trình đào tạo, bồi dưỡng</a>
                                <a class="dropdown-item" href="{{ url("viec-lam?cat_id=67&detail_id=67") }}">Tuyển công chức, viên chức</a>
                                <a class="dropdown-item" href="{{ url("viec-lam?cat_id=70&detail_id=70") }}">Việc làm trong doanh nghiệp</a>
                                <a class="dropdown-item" href="{{ url("viec-lam?cat_id=73&detail_id=73") }}">Tuyển sinh du học</a>
                                <a class="dropdown-item" href="{{ url("viec-lam?cat_id=76&detail_id=76") }}">Tuyển thực tập sinh</a>
                                <a class="dropdown-item" href="{{ url("viec-lam?cat_id=79&detail_id=79") }}">Tuyển nhân lực xuất khẩu lao động</a>
                                <a class="dropdown-item" href="{{ url("nhan-luc?cat_id=82&detail_id=82") }}">Tuyển nhân lực làm việc ngắn hạn</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/bai-viet/lien-he') }}">Liên hệ</a>
                        </li>
                    </ul>
                </div>
                <div class="top-menu-right">
                    <div class="input-form">
                        <form class="form-inline my-2 my-lg-0">
                            <input type="text" name="search" id="home-search-input" placeholder="Tìm kiếm...">
                        </form>
                    </div>
                    <div class="login-info">
                        <span class="search-trigger">
                            <a href="{{ route('login') }}" class="btn btn-primary login-button auth-button">
                                Đăng nhập </a>
                            <a href="{{ route('register') }}" class="btn btn-primary signup-button auth-button">
                                Đăng ký </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
<div class="hspace"></div>
@yield('content')
<div class="container">
    <div class="row">
        <div class="col-md-3 right-padding5">
            <div class="flex-box-ads flex-content_ads">
                <div class="ads-contact">
                    <a href="{{ url('/bai-viet/quang-cao') }}"><img src="{{ asset('images/ads/qc-ct.jpg?v=1.1') }}" alt=""></a>
                </div>
                <div class="ads-contact">
                    <a href="{{ url('/bai-viet/quang-cao') }}"><img src="{{ asset('images/ads/qc-ct-1.jpg?v=1.1') }}" alt=""></a>
                </div>
            </div>

        </div>
        <div class="col-md-6 left-padding5 right-padding5 center-ads">
            <div class="flex-box-ads">
                <div class="ads-contact">
                    <a href="{{ url('/bai-viet/quang-cao') }}"><img src="{{ asset('images/ads/qc-ct-2.jpg?v=1.1') }}" alt=""></a>
                </div>
                <div class="ads-contact">
                    <a href="{{ url('/bai-viet/quang-cao') }}"><img src="{{ asset('images/ads/qc-ct-3.jpg?v=1.1') }}" alt=""></a>
                </div>
                <div class="ads-contact">
                    <a href="{{ url('/bai-viet/quang-cao') }}"><img src="{{ asset('images/ads/qc-ct.jpg?v=1.1') }}" alt=""></a>
                </div>
                <div class="ads-contact">
                    <a href="{{ url('/bai-viet/quang-cao') }}"><img src="{{ asset('images/ads/qc-ct-1.jpg?v=1.1') }}" alt=""></a>
                </div>
            </div>
        </div>
        <div class="col-md-3 left-padding5">
            <div class="flex-box-ads flex-content_ads">
                <div class="ads-contact">
                    <a href="{{ url('/bai-viet/quang-cao') }}"> <img src="{{ asset('images/ads/qc-ct-2.jpg?v=1.1') }}" alt=""></a>
                </div>
                <div class="ads-contact">
                    <a href="{{ url('/bai-viet/quang-cao') }}"> <img src="{{ asset('images/ads/qc-ct-3.jpg?v=1.1') }}" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg animation-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Thông tin chi tiết</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container">
                <div class="row education-info">
                    <div class="col-md-5">Tên ngành đào tạo:</div>
                    <div class="col-md-7">Kỹ thuật trắc địa - bản đồ</div>
                </div>
                <div class="row education-info">
                    <div class="col-md-5">Chỉ tiêu tuyển sinh năm 2020:</div>
                    <div class="col-md-7">5,607</div>
                </div>
                <div class="row education-info">
                    <div class="col-md-5">Chỉ tiêu tuyển sinh năm 2021:</div>
                    <div class="col-md-7">15,700</div>
                </div>
                <div class="row education-info">
                    <div class="col-md-5">Thời gian tuyển sinh</div>
                    <div class="col-md-7">từ 21/7/2020 đến 30/9/2020</div>
                </div>
                <div class="row education-info">
                    <div class="col-md-5">Hình thức, hồ sơ, đăng kí dự tuyển</div>
                    <div class="col-md-7">Cao đẳng, Đại học</div>
                </div>
                <div class="row education-info">
                    <div class="col-md-5">Tư vấn hỗ trợ đăng kí tuyển sinh</div>
                    <div class="col-md-7">0934571487</div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          </div>
        </div>
    </div>
</div>
<div class="container">
    <div id="footer">
        <p style="
            font-weight: 500;
        ">TRƯỜNG ĐẠI HỌC TÀI NGUYÊN VÀ MÔI TRƯỜNG HÀ NỘI</p>
        <p><b>Địa chỉ: </b>Số 41A Đường Phú Diễn - P. Phú Diễn - Q. Bắc Từ Liêm - TP. Hà Nội</p>
        <p><b>Điện thoại: </b> 0243.8385149</p>
        <p><b>Di động: </b> 0983.054.815</p>
        <p><b>Email: </b> ttdtcbcc@gmail.com</p>
    </div>
</div>

<!-- Optional JavaScript -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
</body>
</html>
