<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JobController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\HumanController;
use App\Http\Controllers\EducateController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\Home\HomeController;
use App\Http\Controllers\Admin\Account\RegisterController;

Route::get('/', array('uses' => 'HomeController@index', 'as' => 'home'));
Route::get('dao-tao', [EducateController::class, 'index']);
Route::get('/dang-nhap', ['uses' => 'LoginController@login', 'as' => 'login']);
Route::get('/dang-ky', ['uses' => 'RegisterController@register', 'as' => 'register']);
Route::get('nhan-luc',[HumanController::class, 'index']);
Route::get('viec-lam', [JobController::class, 'index']);
Route::get('/nhan-luc/chi-tiet/nganh-dao-tao', [EducateController::class, 'cateEducate']);
Route::get('bai-viet/{slug}', [PostController::class, 'index']);
Route::get('tin-tuc/{id}', array('uses' => 'PostController@tinTuyenSinh', 'as' => 'news'));
Route::get('/nhan-luc/chi-tiet/gioi-thieu', [AboutController::class, 'index']);


// route admin

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'check_admin_login'], function () {
    Route::get('login', [LoginController::class, 'getLogin'])->name('admin.get.login');
    Route::post('login', [LoginController::class, 'postLogin'])->name('admin.post.login');

    Route::group(['prefix' => 'account', 'namespace' => 'Account'], function () {
        Route::get('register', [RegisterController::class, 'getRegister']);
        Route::post('register', [RegisterController::class, 'postRegister']);
    });

    Route::group(['prefix' => 'home', 'namespace' => 'Home'], function () {
        Route::get('index', [HomeController::class, 'index'])->name('admin.home.index');
    });
});
